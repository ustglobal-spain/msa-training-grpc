package com.ustglobal.spain.msa.training.grpc;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.ustglobal.spain.msa.training.grpc.generated.CustomerServiceGrpc;
import com.ustglobal.spain.msa.training.grpc.generated.GetCustomerRequest;
import com.ustglobal.spain.msa.training.grpc.generated.GetCustomerResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLException;
import java.io.File;

@Slf4j
public class ClientApplication {

    public static void main(String[] args) throws SSLException {

        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);

        ClientApplication app = new ClientApplication();
        app.run();
    }

    public void run() throws SSLException {

        File cert = new File(this.getClass().getClassLoader().getResource("certs/cert.pem").getFile());

        ManagedChannel channel = NettyChannelBuilder.forAddress("localhost", 8080)
                .sslContext(GrpcSslContexts.forClient().trustManager(cert).build())
                .build();

        CustomerServiceGrpc.CustomerServiceBlockingStub service = CustomerServiceGrpc.newBlockingStub(channel);

        GetCustomerRequest request = GetCustomerRequest.newBuilder().setId("1").build();
        GetCustomerResponse response = service.getCustomer(request);

        log.info(response.toString());

        channel.shutdown();
    }
}
