package com.ustglobal.spain.msa.training.grpc;

import com.ustglobal.spain.msa.training.grpc.generated.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLException;
import java.io.File;
import java.util.Iterator;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

@Slf4j
public class TimeApplication {

    public static void main(String[] args) throws InterruptedException, SSLException {

        Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);

        TimeApplication app = new TimeApplication();
        app.run();
    }

    public void run() throws SSLException {

        File cert = new File(this.getClass().getClassLoader().getResource("certs/cert.pem").getFile());

        ManagedChannel channel = NettyChannelBuilder.forAddress("localhost", 8080)
                .sslContext(GrpcSslContexts.forClient().trustManager(cert).build())
                .build();

        TimeServiceGrpc.TimeServiceBlockingStub service = TimeServiceGrpc.newBlockingStub(channel);
        GetTimeRequest request = GetTimeRequest.newBuilder().build();
        Iterator<GetTimeResponse> iter = service.getTime(request);

        while(true) {
            if (iter.hasNext()) {
                GetTimeResponse time = iter.next();
                log.info(time.getTime());
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //channel.shutdown();
    }
}
