package com.ustglobal.spain.msa.training.grpc.services;

import com.ustglobal.spain.msa.training.grpc.generated.*;
import io.grpc.stub.StreamObserver;

import java.time.Instant;

public class TimeServiceImpl extends TimeServiceGrpc.TimeServiceImplBase {

    @Override
    public void getTime(GetTimeRequest request, StreamObserver<GetTimeResponse> responseObserver) {

        while(true) {
            GetTimeResponse response = GetTimeResponse.newBuilder()
                    .setTime(Instant.now().toString())
                    .build();

            responseObserver.onNext(response);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
