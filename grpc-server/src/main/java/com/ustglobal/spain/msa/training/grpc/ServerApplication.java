package com.ustglobal.spain.msa.training.grpc;

import com.ustglobal.spain.msa.training.grpc.services.CustomerServiceImpl;
import com.ustglobal.spain.msa.training.grpc.services.TimeServiceImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public class ServerApplication {

    private static int GRPC_PORT = 8080;

    public static void main(String[] args) throws IOException, InterruptedException {

        ServerApplication app = new ServerApplication();
        app.start();

    }

    public void start() throws IOException, InterruptedException {

        File key = new File(this.getClass().getClassLoader().getResource("certs/key.pem").getFile());
        File cert = new File(this.getClass().getClassLoader().getResource("certs/cert.pem").getFile());

        Server server = ServerBuilder
                .forPort(GRPC_PORT)
                .useTransportSecurity(cert, key)
                .addService(new CustomerServiceImpl())
                .addService(new TimeServiceImpl())
                .build();

        log.info("starting GRPC server on port {}", GRPC_PORT);

        server.start();
        server.awaitTermination();
    }

}
