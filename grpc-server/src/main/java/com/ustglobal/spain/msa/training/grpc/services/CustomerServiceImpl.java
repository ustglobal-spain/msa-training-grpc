package com.ustglobal.spain.msa.training.grpc.services;

import com.ustglobal.spain.msa.training.grpc.generated.*;
import io.grpc.stub.StreamObserver;

public class CustomerServiceImpl extends CustomerServiceGrpc.CustomerServiceImplBase {

    @Override
    public void getCustomer(GetCustomerRequest request, StreamObserver<GetCustomerResponse> responseObserver){

        if (request.getId().equals("1")) {

            CustomerAddress customerAddress = CustomerAddress.newBuilder()
                    .setStreet("Santa Leonor")
                    .setCity("Madrid")
                    .setCountry("Spain")
                    .setZipcode("28080")
                    .build();

            Customer customer = Customer.newBuilder()
                    .setName("Jack")
                    .setPhone("609445566")
                    .setId("1")
                    .setAddress(customerAddress)
                    .build();

            GetCustomerResponse response = GetCustomerResponse.newBuilder()
                    .setCustomer(customer)
                    .build();

            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
    }
}
